library("ggplot2")

IntelXeon <- read.table(file = "ProcV4.tsv" , sep ="\t", header = TRUE)
IntelXeon$cores <- as.factor(IntelXeon$cores)

ggplot(data = IntelXeon, mapping = aes(x = v4, y = freq.base, color= cores)) + 
  geom_point(aes(size=price)) + theme(axis.text.x = element_text(angle = 90))

IntelSkylake <- read.table(file = "ProcSKY.tsv", sep = "\t", header = TRUE)
IntelSkylake$cores <- as.factor(IntelSkylake$cores)

ggplot(data = IntelSkylake, mapping = aes(x = skylake, y = freq.base, color= cores)) + 
  geom_point(aes(size=price)) + theme(axis.text.x = element_text(angle = 90))

mean(IntelSkylake$freq.base)
mean(IntelSkylake$freq.max)


#setwd("~/Documentos")
#tusDatos <- read.table(file = "ProcV4.tsv" , sep ="\t", header = TRUE)



#ggplot(data = tusDatos, mapping = aes(x = v4, y = price))+ 
#  geom_point() 

#table(tusDatos)
#mean(tusDatos)
